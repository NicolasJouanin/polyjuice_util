# Copyright 2021 Nicolas Jouanin <nico@beerfactory.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
defmodule Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier do
  @type t :: %__MODULE__{
          sigil: String.t(),
          opaque_id: String.t(),
          domain: String.t()
        }
  alias Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier

  @enforce_keys [:sigil, :opaque_id, :domain]

  defstruct [
    :sigil,
    :opaque_id,
    :domain
  ]

  @sigil "#"

  @behaviour Polyjuice.Util.Identifiers

  def new!(identifier) do
    case parse(identifier) do
      {:ok, roomAlias} -> roomAlias
      {:error, error} -> raise ArgumentError, error
    end
  end

  @impl Polyjuice.Util.Identifiers
  def new([opaque_id, domain]), do: new(opaque_id, domain)

  @impl Polyjuice.Util.Identifiers
  def new({opaque_id, domain}), do: new(opaque_id, domain)

  @impl Polyjuice.Util.Identifiers
  def new(%{opaque_id: opaque_id, domain: domain}), do: new(opaque_id, domain)

  @impl Polyjuice.Util.Identifiers
  def new(identifier), do: parse(identifier)

  @spec new(String.t(), String.t()) :: {:ok, RoomAliasIdentifier.t()} | {:error, String.t()}
  def new(opaque_id, domain) do
    roomAlias = %RoomAliasIdentifier{sigil: @sigil, opaque_id: opaque_id, domain: domain}

    if valid?(roomAlias) do
      {:ok, roomAlias}
    else
      {:error, :invalid_room_alias_id}
    end
  end

  @impl Polyjuice.Util.Identifiers
  def valid?(%RoomAliasIdentifier{} = this) do
    String.length(this.domain) > 0 && String.length(this.opaque_id) > 0
  end

  @impl Polyjuice.Util.Identifiers
  def valid?(identifier_string) do
    case parse(identifier_string) do
      {:ok, _} -> true
      _ -> false
    end
  end

  def fqid(%RoomAliasIdentifier{} = this) do
    "#{@sigil}#{this.opaque_id}:#{this.domain}"
  end

  @impl Polyjuice.Util.Identifiers
  @spec parse(String.t()) :: {:ok, RoomAliasIdentifier.t()} | {:error, String.t()}
  def parse(id) do
    with @sigil <> id <- id,
         [opaque_id, domain] = String.split(id, ":", parts: 2, trim: true) do
      new(opaque_id, domain)
    else
      error -> {:error, error}
    end
  end
end

defimpl String.Chars, for: Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier do
  def to_string(this), do: Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier.fqid(this)
end
