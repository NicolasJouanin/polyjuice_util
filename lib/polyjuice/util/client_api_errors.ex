# Copyright 2021 Nicolas Jouanin <nico@beerfactory.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
defmodule Polyjuice.Util.ClientAPIErrors do
  import Polyjuice.Util.ClientError

  # Errors defined in https://matrix.org/docs/spec/client_server/r0.6.1#api-standards

  # Common error codes
  deferror(MForbidden)
  deferror(MUnknownToken)
  deferror(MMissingToken)
  deferror(MBadJson)
  deferror(MNotJson)
  deferror(MNotFound)
  deferror(MLimitExceeded)
  deferror(MUnkown)

  # Other error codes
  deferror(MUnrecognized)
  deferror(MUnauthorized)
  deferror(MUserDeactivated)
  deferror(MUserInUse)
  deferror(MInvalidUsername)
  deferror(MRoomInUse)
  deferror(MInvalidRoomState)
  deferror(MThreepidInUse)
  deferror(MThreepidNotFound)
  deferror(MThreepidAuthFailed)
  deferror(MThreepidDenied)
  deferror(MServerNotTrusted)
  deferror(MUnsupportedRoomVersion)
  deferror(MIncompatibleRoomVersion)
  deferror(MBadState)
  deferror(MGuestAccessForbidden)
  deferror(MCaptchaNeeded)
  deferror(MCaptchaInvalid)
  deferror(MMissingParam)
  deferror(MInvalidParam)
  deferror(MTooLarge)
  deferror(MResourceLimitExceeded)
  deferror(MCannotLeaveServerNoticeRoom)
end
