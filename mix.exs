defmodule PolyjuiceUtil.MixProject do
  use Mix.Project

  def project do
    [
      app: :polyjuice_util,
      description: "Utility functions for the Matrix.org communications protocol",
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Polyjuice Util",
      source_url: "https://gitlab.com/uhoreg/polyjuice_util",
      # homepage_url: "http://YOUR_PROJECT_HOMEPAGE",
      docs: [
        # The main page in the docs
        main: "readme",
        # logo: "path/to/logo.png",
        extras: ["README.md"]
      ],
      package: [
        maintainers: ["Hubert Chathi"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/uhoreg/polyjuice_util"
        }
      ],

      # Tests
      test_coverage: [
        summary: [threshold: false]
      ],
      consolidate_protocols: Mix.env() != :test
    ]
  end

  def application do
    [
      extra_applications: [:logger, :crypto]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:uuid, "~> 1.1"},
      {:jason, "~> 1.2"}
    ]
  end
end
