defmodule Polyjuice.UtilTest do
  use ExUnit.Case
  doctest Polyjuice.Util

  test "escape localpart" do
    import Polyjuice.Util, only: [escape_localpart: 2, escape_localpart: 1]
    assert escape_localpart("a") == "a"
    assert escape_localpart("a_") == "a__"
    assert escape_localpart("a_", fold_case: true) == "a_"
  end
end
