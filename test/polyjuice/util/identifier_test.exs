defmodule Polyjuice.Util.IdentfierTest do
  use ExUnit.Case
  doctest Polyjuice.Util.Identifiers

  alias Polyjuice.Util.Identifiers
  alias Polyjuice.Util.Identifiers.V1.UserIdentifier
  alias Polyjuice.Util.Identifiers.V1.RoomIdentifier
  alias Polyjuice.Util.Identifiers.V1.EventIdentifier
  alias Polyjuice.Util.Identifiers.V1.RoomAliasIdentifier
  alias Polyjuice.Util.Identifiers.V1.GroupIdentifier
  doctest Polyjuice.Util.Identifiers

  test "Create new user identifier" do
    assert {:ok, id} = UserIdentifier.new("localpart", "domain")
    assert %UserIdentifier{sigil: "@", localpart: "localpart", domain: "domain"} = id
    assert "#{id}" == "@localpart:domain"
  end

  test "Create V0 user identifier" do
    alias Polyjuice.Util.Identifiers.V0.UserIdentifier
    assert {:ok, id} = UserIdentifier.new("!oca!@", "domain")
    assert %UserIdentifier{sigil: "@", localpart: "!oca!@", domain: "domain"} = id
    assert "#{id}" == "@!oca!@:domain"
  end

  test "Create new room identifier" do
    assert {:ok, id} = RoomIdentifier.new("opaque_id", "domain")
    assert %RoomIdentifier{sigil: "!", opaque_id: "opaque_id", domain: "domain"} = id
    assert "#{id}" == "!opaque_id:domain"
  end

  test "Create new event V1 identifier" do
    assert {:ok, id} = EventIdentifier.new("opaque_id", "domain")
    assert %EventIdentifier{sigil: "$", opaque_id: "opaque_id", domain: "domain"} = id
    assert "#{id}" == "$opaque_id:domain"
  end

  test "Create new room alias identifier" do
    assert {:ok, id} = RoomAliasIdentifier.new("opaque_id", "domain")
    assert %RoomAliasIdentifier{sigil: "#", opaque_id: "opaque_id", domain: "domain"} = id
    assert "#{id}" == "#opaque_id:domain"
  end

  test "Generate user identifier" do
    assert id = UserIdentifier.generate("domain")
    assert %UserIdentifier{localpart: localpart, domain: "domain"} = id
    assert localpart != ""
  end

  test "Fail create invalid user identifier" do
    assert {:error, :invalid_user_id} = UserIdentifier.new("INVALID", "domain")
  end

  test "Valid user identifier" do
    assert UserIdentifier.valid?("@localpart:domain")
  end

  test "Invalid user identifier" do
    assert !UserIdentifier.valid?("@LOCALPART:domain")
  end

  test "Generate room identifier" do
    assert id = RoomIdentifier.generate("domain")
    assert %RoomIdentifier{opaque_id: localpart, domain: "domain"} = id
    assert localpart != ""
  end
end
