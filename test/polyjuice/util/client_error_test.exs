defmodule Polyjuice.Util.ClientErrorTest do
  use ExUnit.Case
  import Polyjuice.Util.ClientError
  import Polyjuice.Util.ClientAPIErrors
  doctest Polyjuice.Util.ClientError

  test "map error with nil defaults" do
    deferror(TestError1)
    test_error = TestError1.new()
    assert test_error.errcode == "TEST_ERROR1"
    assert is_nil(test_error.error)
    assert is_nil(test_error.additional_fields)
  end

  test "map error with macro defaults" do
    deferror(TestError2, "error message", [{"test", "test"}])
    test_error = TestError2.new()
    assert test_error.errcode == "TEST_ERROR2"
    assert !is_nil(test_error.error)
    assert !is_nil(test_error.additional_fields)
  end

  test "map error with error message" do
    deferror(TestError3)
    test_error = TestError3.new("error")
    assert test_error.errcode == "TEST_ERROR3"
    assert !is_nil(test_error.error)
    assert is_nil(test_error.additional_fields)
  end

  test "JSON error encode" do
    deferror(JsonTestError)
    test_error = JsonTestError.new("ERROR", [{"test", "test"}])
    assert {:ok, encoded} = json_encode(test_error)
    assert encoded == "{\"errcode\":\"JSON_TEST_ERROR\",\"error\":\"ERROR\",\"test\":\"test\"}"
  end

  test "JSON encoded MForbidden error" do
    assert MForbidden.new() |> json_encode!() == "{\"errcode\":\"M_FORBIDDEN\"}"
  end
end
