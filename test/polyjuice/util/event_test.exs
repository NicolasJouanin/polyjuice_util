defmodule Polyjuice.Util.EventTest do
  use ExUnit.Case
  doctest Polyjuice.Util.Event

  test "redaction" do
    assert Polyjuice.Util.Event.redact(
             "2",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "3",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "4",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "5",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.message",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "membership" => "join"
               },
               "state_key" => "@user:example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.member",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "membership" => "join"
               },
               "state_key" => "@user:example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.member",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "creator" => "@user:example.com"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.create",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "creator" => "@user:example.com"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.create",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "join_rule" => "public"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.join_rules",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "join_rule" => "public"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.join_rules",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "ban" => 50,
                 "events" => %{
                   "m.room.name" => 100,
                   "m.room.power_levels" => 100
                 },
                 "events_default" => 0,
                 "invite" => 50,
                 "kick" => 50,
                 "notifications" => %{
                   "room" => 20
                 },
                 "redact" => 50,
                 "state_default" => 50,
                 "users" => %{
                   "@example:localhost" => 100
                 },
                 "users_default" => 0
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.power_levels",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "ban" => 50,
                 "events" => %{
                   "m.room.name" => 100,
                   "m.room.power_levels" => 100
                 },
                 "events_default" => 0,
                 "kick" => 50,
                 "redact" => 50,
                 "state_default" => 50,
                 "users" => %{
                   "@example:localhost" => 100
                 },
                 "users_default" => 0
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.power_levels",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "aliases" => ["#aroom:example.com", "#anothername:example.com"]
               },
               "state_key" => "example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.aliases",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "aliases" => ["#aroom:example.com", "#anothername:example.com"]
               },
               "state_key" => "example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.aliases",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content",
                 "history_visibility" => "invited"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.history_visibility",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{
                 "history_visibility" => "invited"
               },
               "state_key" => "",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "type" => "m.room.history_visibility",
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "1",
             %{
               "content" => %{
                 "body" => "Here is the message content"
               },
               "state_key" => "example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "content" => %{},
               "state_key" => "example.com",
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }

    assert Polyjuice.Util.Event.redact(
             "1",
             %{
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{},
               "unsigned" => %{
                 "age_ts" => 1_000_000
               }
             }
           ) == {
             :ok,
             %{
               "event_id" => "$0:domain",
               "origin" => "domain",
               "origin_server_ts" => 1_000_000,
               "room_id" => "!r:domain",
               "sender" => "@u:domain",
               "signatures" => %{}
             }
           }
  end

  test "compute_content_hash" do
    {:ok, hash1} =
      Polyjuice.Util.Event.compute_content_hash(
        "1",
        %{
          "event_id" => "$0:domain",
          "origin" => "domain",
          "origin_server_ts" => 1_000_000,
          "room_id" => "!r:domain",
          "sender" => "@u:domain",
          "hashes" => %{},
          "signatures" => %{},
          "unsigned" => %{
            "age_ts" => 1_000_000
          }
        }
      )

    {:ok, hash2} =
      Polyjuice.Util.Event.compute_content_hash(
        "1",
        %{
          "event_id" => "$0:domain",
          "origin" => "domain",
          "origin_server_ts" => 1_000_000,
          "room_id" => "!r:domain",
          "sender" => "@u:domain"
        }
      )

    assert hash1 == hash2
  end
end
