# Polyjuice Util

Utility functions for using the [Matrix protocol](https://matrix.org/).  These
are functions that are used by multiple components (e.g. clients, application
services, identity services, homeservers).

## Installation

The package can be installed by adding `polyjuice_util` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:polyjuice_util, "~> 0.1.0"}
  ]
end
```
